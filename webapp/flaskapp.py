from flask import Flask
from flask_wtf.csrf import CSRFProtect

from flask import Flask
import json
import os
import boto3
from . import models
from . import views


class AppEnv:
    def __init__(self):
        self.app = None
        self.models = None
        self.boto3 = None
        self.aws_config = None
        return


appenv = AppEnv()
appenv.app = Flask(__name__)
appenv.app.config['SECRET_KEY'] = 'foobar'
CSRFProtect(appenv.app)

appenv.boto3 = boto3

# Load the database/models
appenv.models = models.ModelAPI(appenv)

# Load the views
views.init(appenv)
