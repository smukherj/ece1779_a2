# Import common utilities all views could use
from urllib import unquote_plus
from flask import render_template
from flask import request
from flask import redirect
from flask import session
from flask import url_for
import flask
import os
from boto3.dynamodb.conditions import Key as _Key
from boto3.dynamodb.conditions import Attr as _Attr


app = None
models = None
appenv = None


class User:
    def __init__(self, json_user):
        self.login = json_user['login']
        self.password = json_user['password']
        self.email = json_user['email']
        return


def _get_table(name):
    db = appenv.boto3.resource('dynamodb')
    return db.Table(name)


def get_user(login):
    accounts = _get_table('Accounts')
    json_user = accounts.get_item(Key={'login': login}).get('Item')
    if json_user is not None:
        return User(json_user)
    return


def register_user(login, password, email):
    accounts = _get_table('Accounts')
    accounts.put_item(
        Item={
            'login': login,
            'password': password,
            'email': email
        }
    )


def set_solution(d):
    solutions = _get_table('Solutions')
    solutions.put_item(
        Item=d
    )


def get_base_name(name):
    table = _get_table('Crosswords')
    entry = table.get_item(Key={'puzzle_name': name}).get('Item')
    return entry['crossword']


def get_solution(name):
    solutions = _get_table('Solutions')
    base_name = get_base_name(name)
    entry = solutions.get_item(Key={'name': base_name}).get('Item')
    return entry['data']


def get_dimensions(name):
    solutions = _get_table('Solutions')
    base_name = get_base_name(name)
    entry = solutions.get_item(Key={'name': base_name}).get('Item')
    return (entry['num_rows'], entry['num_cols'])


def _stringify_crossword_names(response, field):
    return [str(item[field]) for item in response['Items']]


def _list_user_crosswords(login):
    table = _get_table('UserCrossword')
    field = 'puzzle_name'
    response = table.query(
        ProjectionExpression=field,
        KeyConditionExpression=_Key('login').eq(login)
    )
    return _stringify_crossword_names(response, field)


def _list_all_crosswords():
    table = _get_table('Crosswords')
    field = 'puzzle_name'
    response = table.scan(ProjectionExpression=field)
    return _stringify_crossword_names(response, field)


def _filter_open_crosswords(crosswords):
    table = _get_table('Crosswords')
    return [name for name in crosswords \
        if 'closed' not in table.get_item(Key={'puzzle_name': name}).get('Item')]


def list_crosswords(login=None):
    if login is None:
        return _filter_open_crosswords(_list_all_crosswords())
    else:
        return _filter_open_crosswords(_list_user_crosswords(login))


def list_completed_crosswords(login):
    crosswords = _list_user_crosswords(login)
    table = _get_table('Crosswords')
    return [name for name in crosswords \
        if 'closed' in table.get_item(Key={'puzzle_name': name}).get('Item')]


def list_users_for_puzzle(puzzle_id):
    table = _get_table('UserCrossword')
    resp = table.scan(
        ProjectionExpression='login',
        FilterExpression=_Attr('puzzle_name').eq(puzzle_id)
    )
    return _stringify_crossword_names(resp, 'login')


def list_crossword_templates():
    table = _get_table('Solutions')
    field = 'name'
    response = table.scan(
        ProjectionExpression='#name',
        ExpressionAttributeNames={
            '#name': field})
    return sorted(_stringify_crossword_names(response, field), reverse=True)


def input_to_crossword(login, name, index, val):
    # Cannot input empty string, so use '--' to represent deleted
    if val == '':
        val = '--'
    table = _get_table('Crosswords')
    entry = table.get_item(Key={'puzzle_name': name}).get('Item')
    if index in entry and entry[index] == val:
        # No change, don't update any tables
        return

    update_expression = 'set ' + index + ' = :v'
    table.update_item(Key={'puzzle_name': name},
        UpdateExpression=update_expression,
        ExpressionAttributeValues={':v': val})
    # Update individual entry
    user_table = _get_table('UserCrossword')
    user_table.update_item(Key={'login': login, 'puzzle_name': name},
        UpdateExpression=update_expression,
        ExpressionAttributeValues={':v': val})


def get_crossword_inputs(name):
    table = _get_table('Crosswords')
    item = table.get_item(Key={'puzzle_name': name})
    return item.get('Item')


def get_user_crossword_inputs(name, login):
    user_table = _get_table('UserCrossword')
    item = user_table.get_item(
        Key={
            'login': login,
            'puzzle_name': name})
    return item.get('Item')


def get_all_user_inputs(name):
    user_table = _get_table('UserCrossword')
    resp = user_table.scan(
        FilterExpression=_Key('puzzle_name').eq(name)
    )
    return resp.get('Items')


def join_crossword(puzzle_name, login):
    table = _get_table('UserCrossword')
    table.put_item(
        Item={
            'login': login,
            'puzzle_name': puzzle_name
        }
    )


def create_crossword(puzzle_name, crossword, mode, login):
    table = _get_table('Crosswords')
    table.put_item(
        Item={
            'puzzle_name': puzzle_name,
            'crossword': crossword,
            'mode': mode,
            'creator': login
        }
    )
    join_crossword(puzzle_name, login)


def close_crossword(name):
    table = _get_table('Crosswords')
    entry = table.get_item(Key={'puzzle_name': name}).get('Item')
    entry['closed'] = True
    table.put_item(Item=entry)


def add_user_stats(login, puzzle_name, global_stats, user_stats):
    base_name = get_base_name(puzzle_name)
    stats_table = _get_table('UserStats')
    item = stats_table.get_item(
        Key={
            'login': login}
    ).get('Item')
    put_item = False
    if not item:
        put_item = True
        item = stats_table.put_item(
            Item={
                'login': login,
                'puzzles': {base_name},
                'total': 0,
                'group_correct': 0,
                'group_wrong': 0,
                'group_unattempted': 0,
                'user_correct': 0,
                'user_wrong': 0,
                'user_unattempted': 0,
                'user_overriden': 0,
                'user_corrected': 0
            }
        )
        item = stats_table.get_item(
            Key={
                'login': login}
        ).get('Item')
    if base_name not in item['puzzles'] or put_item:
        item['puzzles'].add(base_name)
        item['total'] += global_stats[3]
        item['group_correct'] += global_stats[0]
        item['group_wrong'] += global_stats[1]
        item['group_unattempted'] += global_stats[2]
        item['user_correct'] += user_stats[0]
        item['user_wrong'] += user_stats[1]
        item['user_unattempted'] += user_stats[2]
        item['user_overriden'] += user_stats[3]
        item['user_corrected'] += user_stats[4]
        stats_table.put_item(Item=item)


def get_user_stats(login):
    stats_table = _get_table('UserStats')
    return stats_table.get_item(
        Key={
            'login': login}
    ).get('Item')

def list_users_for_leaderboard():
    table = _get_table('UserStats')
    response = table.scan(ProjectionExpression='login')
    return _stringify_crossword_names(response, 'login')

def _filter_puzzle_id_character(id_char):
    if id_char >= 'a' and id_char <= 'z':
        return True
    elif id_char >= '0' and id_char <= '9':
        return True
    elif id_char in ['-', '_']:
        return True
    return False


def filter_puzzle_id(puzzle_id):
    puzzle_id = str(puzzle_id).lower()
    puzzle_id = filter(_filter_puzzle_id_character, puzzle_id)
    return puzzle_id


def get_kloo_s3_bucket():
    return 'ece1779a2-crossword-clues'


def get_kloo_key(puzzle_id):
    base_name = get_base_name(puzzle_id)
    filtered_name = filter_puzzle_id(base_name)
    return os.path.join(filtered_name, 'kloo.jpg')


def get_kloo_link(puzzle_id):
    url = 'https://s3.amazonaws.com/{}/{}'.format(
        get_kloo_s3_bucket(), get_kloo_key(puzzle_id))
    return url


