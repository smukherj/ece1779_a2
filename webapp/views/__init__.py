
def init(appenv):
    # Set up the app and models
    # so that other views can
    # access them
    from . import views_common as vc
    vc.app = appenv.app
    vc.models = appenv.models
    vc.appenv = appenv

    # Import all views
    from . import index
    from . import register
    from . import login
    from . import home
    from . import upload_solution
    from . import puzzle
    from . import user_stats
