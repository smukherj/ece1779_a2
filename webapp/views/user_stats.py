from .views_common import *


@app.route('/user_stats')
def user_stats():
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)

    stats = get_user_stats(user.login)
    return render_template(
        'user_stats.html',
        user=user,
        stats=stats
    )

@app.route('/view_stats/<login_id>')
def view_stats(login_id):
    user = get_user(login_id)
    stats = get_user_stats(user.login)
    return render_template(
        'user_stats.html',
        user=user,
        stats=stats
    )
