from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Email
from .views_common import *

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = get_user(form.username.data)
        if user is not None:
            return render_template('register.html', 
                    form=form, 
                    register_error='Sorry username %s is taken'%user.login)
        else:
            # User doesn't exist. Commit user details to database and redirect to home
            register_user(form.username.data, form.password.data, form.email.data)
            session['login'] = form.username.data
            return redirect(url_for('home'))
    else:
        return render_template('register.html', form=form, register_error=None)
